import os

database_path = 'D:\\EverynoiseParser'


if __name__ == '__main__':
    genres = 0
    artists = 0
    ignored_artists = 0
    nokey_artists = 0
    relations = 0
    mirrors = 0
    for genre_folder in os.listdir(database_path):
        genres += 1
        genre_path = os.path.join(database_path, genre_folder)

        ignore_path = os.path.join(genre_path, 'ignore.txt')
        if os.path.exists(ignore_path):
            with open(ignore_path, 'r', encoding='utf-8') as fp:
                ignored_artists += len(fp.read().split('\n'))

        nokey_path = os.path.join(genre_path, 'nokey.txt')
        if os.path.exists(nokey_path):
            with open(nokey_path, 'r', encoding='utf-8') as fp:
                nokey_artists += len(fp.read().split('\n'))

        related_path = os.path.join(genre_path, 'related.txt')
        if os.path.exists(related_path):
            with open(related_path, 'r', encoding='utf-8') as fp:
                relations += len(fp.read().split('\n'))

        mirror_path = os.path.join(genre_path, 'mirror.txt')
        if os.path.exists(mirror_path):
            with open(mirror_path, 'r', encoding='utf-8') as fp:
                mirrors += len(fp.read().split('\n'))

        for artist_folder in [f for f in os.listdir(genre_path)
                              if f not in ['name.txt', 'mirror.txt', 'related.txt', 'ignore.txt', 'nokey.txt']]:
            artists += 1
            artist_path = os.path.join(genre_path, artist_folder)
            if len(os.listdir(artist_path)) == 0:
                print('Directory {} is empty, removing...'.format(artist_path))
                os.rmdir(artist_path)
    print('Total genres: {}'.format(genres))
    print('Total artists: {} (+ignored {} {:.3f}%, +nokey: {} {:.3f}%)'
          .format(artists, ignored_artists, ignored_artists / float(artists) * 100,
                  nokey_artists, nokey_artists / float(artists) * 100))
    print('Total relations: {} (avg {:.2f})'.format(relations, float(relations) / genres))
    print('Total mirrors: {} (avg {:.2f})'.format(mirrors, float(mirrors) / genres))
