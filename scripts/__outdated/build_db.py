import json
import os
from typing import Dict, Any, List

from db.db_api import DbApi

database_path = 'D:\\EverynoiseParser'


class DbBuilder:
    def __init__(self, clear: bool = False):
        self.db_api: DbApi = DbApi(clear)

    def run(self):
        genres_folders = os.listdir(database_path)
        total_genres = len(genres_folders)
        genres_processed = 0
        for genre_folder in genres_folders:
            genre_path = os.path.join(database_path, genre_folder)
            with open(os.path.join(genre_path, 'name.txt'), 'r', encoding='utf-8') as fp:
                genre_name = fp.read()
            genres_processed += 1
            print('Processing genre "{}" ({} / {})...'.format(genre_name, genres_processed, total_genres))

            genre = self.db_api.create_genre_if_not_exist_and_get(genre_name)
            with open(os.path.join(genre_path, 'related.txt'), 'r', encoding='utf-8') as fp:
                relations = fp.read().split('\n')
            for relation in relations:
                related_genre = self.db_api.create_genre_if_not_exist_and_get(relation)
                if related_genre not in genre.related_genres:
                    genre.related_genres.append(related_genre)
            with open(os.path.join(genre_path, 'mirror.txt'), 'r', encoding='utf-8') as fp:
                mirrors = fp.read().split('\n')
            for mirror in mirrors:
                mirror_genre = self.db_api.create_genre_if_not_exist_and_get(mirror)
                if mirror_genre not in genre.mirror_genres:
                    genre.mirror_genres.append(mirror_genre)
            for artist_folder in [f for f in os.listdir(genre_path)
                                  if f not in ['name.txt', 'mirror.txt', 'related.txt', 'ignore.txt', 'nokey.txt']]:
                artist_path = os.path.join(genre_path, artist_folder)
                with open(os.path.join(artist_path, 'i.json'), 'r') as fp:
                    info_json = fp.read()
                info: Dict[str, Any] = json.loads(info_json)
                artist_names: List[str] = [artist_info['name'] for artist_info in info['artists']]
                track_name: str = info['name']
                track = self.db_api.create_track_if_not_exist_and_get(track_name)
                track.info = info
                track.preview_url: str = info['preview_url']
                for artist_name in artist_names:
                    artist = self.db_api.create_artist_if_not_exist_and_get(artist_name)
                    if track not in artist.tracks:
                        artist.tracks.append(track)
                    if genre not in artist.genres:
                        artist.genres.append(genre)
            self.db_api.session.commit()


if __name__ == '__main__':
    DbBuilder(clear=True).run()
