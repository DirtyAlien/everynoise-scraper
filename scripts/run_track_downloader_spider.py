from scrapy.crawler import CrawlerProcess
from melpomene.parsing import TrackDownloaderSpider


if __name__ == '__main__':
    process = CrawlerProcess({
        'CONCURRENT_REQUESTS': 32,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 32,
        'AUTOTHROTTLE_ENABLED': True,
        'AUTOTHROTTLE_START_DELAY': .05,
        'AUTOTHROTTLE_MAX_DELAY': .1,
        'LOG_LEVEL': 'INFO',
    })
    process.crawl(TrackDownloaderSpider)
    process.start()
