import tempfile
import os

import pygame
from pygame import mixer

from melpomene.db.db_controller import DbController
from melpomene.db.models import Track

search: str = 'dead'

if __name__ == '__main__':
    pygame.init()
    db_api = DbController()
    sc = db_api.create_non_negotiating_session_context()
    track: Track = sc.tracks.search_by_name(search, limit=1, downloaded=True)[0]
    sc.close()
    db_api.stop_session_expires_check_loop()

    temp_file = tempfile.NamedTemporaryFile('w+b', delete=False, suffix='.mp3')
    temp_file.write(track.preview_file)
    temp_file.close()
    pygame.display.set_mode((256, 64))
    mixer.music.load(temp_file.name)
    mixer.music.play(0)

    clock = pygame.time.Clock()
    while mixer.music.get_busy():
        for event in pygame.event.get():
            if event.type == pygame.QUIT or event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                mixer.music.stop()
        clock.tick(60)

    pygame.quit()
    os.remove(temp_file.name)
