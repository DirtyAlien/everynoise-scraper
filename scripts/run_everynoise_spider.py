from scrapy.crawler import CrawlerProcess
from melpomene.parsing import EverynoiseSpider


if __name__ == '__main__':
    process = CrawlerProcess({
        'CONCURRENT_REQUESTS': 32,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 32,
        'LOG_LEVEL': 'INFO',
    })
    process.crawl(EverynoiseSpider)
    process.start()
