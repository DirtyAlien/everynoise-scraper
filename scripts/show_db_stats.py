from melpomene.db.db_controller import DbController

if __name__ == '__main__':
    db_api = DbController()
    sc = db_api.create_non_negotiating_session_context()

    print('Test genre:')
    genre = sc.genres.get('anime')
    print(genre.name)
    print('Artists: {}'.format([a.name for a in genre.artists]))
    print('Related: {}'.format([r.name for r in genre.related_genres]))
    print('Mirror: {}'.format([r.name for r in genre.mirror_genres]))

    print('\nTest artist:')
    artist = sc.artists.get('Ken Ashcorp')
    print(artist.name)
    print('Genres: {}'.format([g.name for g in artist.genres]))
    print('Tracks: {}'.format([t.name for t in artist.tracks]))

    print('\nTest track:')
    track = sc.tracks.search_by_name('fire', limit=1)[0]
    print(track.name)
    print('Artists: {}'.format([a.name for a in track.artists]))
    print('Genres: {}'.format([g.name for g in track.genres]))
    print('Preview URL: {}'.format(track.preview_url))

    print('\nTotal stats:')
    print('Genres count: {}'.format(sc.genres.get_count()))
    print('Artists count: {}'.format(sc.artists.get_count()))
    print('Tracks count: {}'.format(sc.tracks.get_count()))
    print('Invalid tracks count: {}'.format(sc.tracks.get_invalid_count()))
    print('Tracks to download count: {}'.format(sc.tracks.get_to_download_count()))
    print('Downloaded tracks count: {}'.format(sc.tracks.get_downloaded_count()))

    sc.close()
    db_api.stop_session_expires_check_loop()
