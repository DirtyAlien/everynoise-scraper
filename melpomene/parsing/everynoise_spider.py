from typing import List, Set

from scrapy import Spider, Request
from scrapy.http import Response

from melpomene.db.db_controller import DbController
from melpomene.db.models import Genre
from melpomene.db.session_context import SessionContext

base_url = 'http://everynoise.com'


class EverynoiseSpider(Spider):
    name = 'everynoise.com spider'
    start_urls = [base_url]

    genres_count = None
    genres_parsed = None
    db_api: DbController
    sc: SessionContext
    all_track_ids: List[str] = []

    def parse(self, response: Response):
        self.db_api = DbController()
        self.sc = self.db_api.create_non_negotiating_session_context()

        genre_names: List[str] = \
            [div.xpath('text()').extract()[0] for div in response.xpath('//body/div[@class="canvas"]/div')]
        self.genres_count = len(genre_names)
        self.genres_parsed = 0
        print('Removing outdated genres...')
        for genre in self.sc.genres.get_all():
            if genre.name not in genre_names:
                print('Outdated genre "{}", removing...'.format(genre.name))
                self.sc.session.delete(genre)
        for div in response.xpath('//body/div[@class="canvas"]/div'):
            genre_name = div.xpath('text()').extract()[0]
            genre_page_link = div.xpath('a/@href').extract()[0]
            genre_page_url = '{}/{}'.format(base_url, genre_page_link)
            yield Request(genre_page_url, callback=self.parse_genre, meta={'genre': genre_name})

    def parse_genre(self, response: Response):
        genre_name: str = response.meta['genre']
        # genre: Genre = self.sc.genres.create_if_not_exist_and_get(genre_name)
        # for div in response.xpath('//div[@id="tunnel"]/div[@class="canvas"]/div'):
        #     related_genre_name = div.xpath('text()').extract()[0]
        #     related_genre = self.sc.genres.create_if_not_exist_and_get(related_genre_name)
        #     if related_genre not in genre.related_genres:
        #         genre.related_genres.append(related_genre)
        # for div in response.xpath('//div[@id="mirror"]/div[@class="canvas"]/div'):
        #     mirror_genre_name = div.xpath('text()').extract()[0]
        #     mirror_genre = self.sc.genres.create_if_not_exist_and_get(mirror_genre_name)
        #     if mirror_genre not in genre.mirror_genres:
        #         genre.mirror_genres.append(mirror_genre)
        for div in response.xpath('//body/div[@class="canvas"]/div'):
            track_spotify_id: str = div.xpath('@onclick').extract()[0].split('"')[1]
            if len(track_spotify_id) > 0:
                self.all_track_ids.append(track_spotify_id)
                # track = self.sc.tracks.create_if_not_exist_and_get(track_spotify_id)
                # track.original_genre = genre
        # self.sc.session.commit()
        self.genres_parsed += 1
        print('Genre "{}" parsed ({} / {}).'.format(genre_name, self.genres_parsed, self.genres_count))

    def closed(self, _):
        self.sc.session.commit()
        db_track_spotify_ids = set(self.sc.tracks.get_all_spotify_ids())
        all_track_spotify_ids = set(self.all_track_ids)
        print('Parsed {} tracks. Database contains {} tracks.'
              .format(len(all_track_spotify_ids), len(db_track_spotify_ids)))
        track_spotify_ids_to_remove: Set[str] = db_track_spotify_ids.difference(all_track_spotify_ids)
        print('Removing {} outdated tracks...'.format(len(track_spotify_ids_to_remove)))
        # self.sc.tracks.delete_many_by_spotify_ids(track_spotify_ids_to_remove)
        self.sc.close()
        self.db_api.stop_session_expires_check_loop()
