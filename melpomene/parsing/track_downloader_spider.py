from typing import List

from scrapy import Spider, Request
from scrapy.http import Response

from melpomene.db.db_controller import DbController
from melpomene.db.session_context import SessionContext
from melpomene.db.models import Track


class TrackDownloaderSpider(Spider):
    name = 'track downloader spider'

    db_api: DbController
    sc: SessionContext
    done: int = 0
    not_downloaded_yet_ids: List[int] = []

    def start_requests(self):
        self.db_api = DbController()
        self.sc = self.db_api.create_non_negotiating_session_context()
        while True:
            tracks = self.sc.tracks.get_many_to_download(limit=128, ignore_ids=self.not_downloaded_yet_ids)
            if len(tracks) == 0:
                print('All tracks downloaded! ({})'.format(self.done))
                break
            else:
                for track in tracks:
                    self.not_downloaded_yet_ids.append(track.id)
                    yield Request(track.preview_url, meta={'track': track}, callback=self.parse,
                                  errback=lambda _: self.not_downloaded_yet_ids.remove(track.id))
                self.sc.session.commit()
                print('Downloaded {} tracks.'.format(self.done))

    def parse(self, response: Response):
        track: Track = response.meta['track']
        self.not_downloaded_yet_ids.remove(track.id)
        track.preview_file = response.body
        self.done += 1

    def closed(self, _):
        self.sc.close()
        self.db_api.stop_session_expires_check_loop()
