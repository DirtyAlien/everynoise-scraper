import json
from typing import List, Any, Dict

from scrapy import Spider, Request
from scrapy.http import Response

from melpomene.db.db_controller import DbController
from melpomene.db.models import Track, Genre
from melpomene.db.session_context import SessionContext

base_url = 'http://everynoise.com'


class InfoDownloaderSpider(Spider):
    name = 'info downloader spider'

    db_api: DbController
    sc: SessionContext
    done: int = 0
    not_downloaded_yet_ids: List[int]

    def start_requests(self):
        self.db_api = DbController()
        self.sc = self.db_api.create_non_negotiating_session_context()
        self.not_downloaded_yet_ids = []
        while True:
            tracks = self.sc.tracks.get_many_invalid(limit=128, ignore_ids=self.not_downloaded_yet_ids)
            if len(tracks) == 0:
                print('All tracks info downloaded! ({})'.format(self.done))
                break
            else:
                for track in tracks:
                    self.not_downloaded_yet_ids.append(track.id)
                    link = '{}/spotproxy.cgi?track={}'.format(base_url, track.spotify_id)
                    yield Request(link, meta={'track': track}, callback=self.parse,
                                  errback=lambda _: self.not_downloaded_yet_ids.remove(track.id))
                self.sc.session.commit()
                print('Downloaded {} tracks info.'.format(self.done))

    def parse(self, response: Response):
        track: Track = response.meta['track']
        self.not_downloaded_yet_ids.remove(track.id)
        genre: Genre = track.original_genre
        info: Dict[str, Any] = json.loads(response.text)
        track.name = info['name']
        track.preview_url = info['preview_url']
        track.info = info
        artist_names: List[str] = [artist_info['name'] for artist_info in info['artists']]
        for artist_name in artist_names:
            artist = self.sc.artists.create_if_not_exist_and_get(artist_name)
            if track not in artist.tracks:
                artist.tracks.append(track)
            if genre not in artist.genres:
                artist.genres.append(genre)
        self.done += 1

    def closed(self, _):
        self.sc.close()
        self.db_api.stop_session_expires_check_loop()
