from melpomene.web.handlers.base import BaseHandler
from melpomene.web.handlers.artists import *
from melpomene.web.handlers.genres import *
from melpomene.web.handlers.tracks import *
from melpomene.web.handlers.users import *
