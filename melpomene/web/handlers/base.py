import json
from typing import Any, Dict

from tornado.web import RequestHandler


class BaseHandler(RequestHandler):
    json: Dict[str, Any]

    def data_received(self, chunk):
        pass

    def prepare(self):
        if self.request.headers.get('Content-Type', '').startswiith('application/json'):
            self.json = json.loads(self.request.body)

    def send_json(self, body: Dict[str, Any]):
        self.add_header('Content-Type', 'application/json')
        self.set_status(200)
        self.finish(json.dumps(body))
