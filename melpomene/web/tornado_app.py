from tornado.ioloop import IOLoop
from tornado.web import Application

from melpomene.web.handlers import *


class TornadoApp:
    def __init__(self):
        handlers = [
            # users
            (r'/users/[0-9]', UsersHandler),
            # genres
            (r'/genres/[0-9]', GenresHandler),
            # artists
            (r'/artists/[0-9]', ArtistsHandler),
            # tracks
            (r'/tracks/[0-9]', TracksHandler),
        ]
        settings = {}

        self.app: Application = Application(handlers, **settings)

    def run(self):
        self.app.listen(8888)
        IOLoop.current().start()
