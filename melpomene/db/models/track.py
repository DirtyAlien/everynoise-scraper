from typing import List

import sqlalchemy as sa
from sqlalchemy.orm import relationship

import melpomene.db.models as models

ArtistTrack = sa.Table(
    'artist_track', models.Base.metadata,
    sa.Column('artist_id', sa.Integer, sa.ForeignKey('artist.id'), primary_key=True),
    sa.Column('track_id', sa.Integer, sa.ForeignKey('track.id'), primary_key=True)
)


class Track(models.Base):
    __tablename__ = 'track'

    id = sa.Column(sa.Integer, primary_key=True)
    spotify_id = sa.Column(sa.String, nullable=False, index=True)
    name = sa.Column(sa.String, index=True)
    info = sa.Column(sa.JSON)
    preview_url = sa.Column(sa.String)
    preview_file = sa.Column(sa.LargeBinary)

    artists: List['models.Artist'] = relationship("Artist", secondary=ArtistTrack, back_populates="tracks")
    original_genre_id = sa.Column(sa.Integer, sa.ForeignKey('genre.id'))
    original_genre: 'models.Genre' = relationship('Genre')

    @property
    def genres(self) -> List['models.Genre']:
        genres = []
        for artist in self.artists:
            genres.extend(artist.genres)
        return genres

    def __init__(self, spotify_id: str):
        self.spotify_id = spotify_id
