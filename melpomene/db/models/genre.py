from typing import List

import sqlalchemy as sa
from sqlalchemy.orm import relationship

import melpomene.db.models as models

GenreRelations = sa.Table(
    'genre_relations', models.Base.metadata,
    sa.Column('genre_a_id', sa.Integer, sa.ForeignKey('genre.id'), primary_key=True),
    sa.Column('genre_b_id', sa.Integer, sa.ForeignKey('genre.id'), primary_key=True)
)

GenreMirrors = sa.Table(
    'genre_mirrors', models.Base.metadata,
    sa.Column('genre_a_id', sa.Integer, sa.ForeignKey('genre.id'), primary_key=True),
    sa.Column('genre_b_id', sa.Integer, sa.ForeignKey('genre.id'), primary_key=True)
)


class Genre(models.Base):
    __tablename__ = 'genre'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String, nullable=False, index=True)

    def __init__(self, name: str):
        self.name = name

    artists: List[models.Artist] = relationship("Artist", secondary=models.GenreArtist, back_populates="genres")
    related_genres: List['Genre'] = relationship(
        "Genre",
        secondary=GenreRelations,
        primaryjoin=id == GenreRelations.c.genre_a_id,
        secondaryjoin=id == GenreRelations.c.genre_b_id)
    mirror_genres: List['Genre'] = relationship(
        "Genre",
        secondary=GenreMirrors,
        primaryjoin=id == GenreMirrors.c.genre_a_id,
        secondaryjoin=id == GenreMirrors.c.genre_b_id)
