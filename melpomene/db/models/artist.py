from typing import List

import sqlalchemy as sa
from sqlalchemy.orm import relationship

import melpomene.db.models as models

GenreArtist = sa.Table(
    'genre_artist', models.Base.metadata,
    sa.Column('genre_id', sa.Integer, sa.ForeignKey('genre.id'), primary_key=True),
    sa.Column('artist_id', sa.Integer, sa.ForeignKey('artist.id'), primary_key=True)
)


class Artist(models.Base):
    __tablename__ = 'artist'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String, nullable=False, index=True)

    genres: List['models.Genre'] = relationship("Genre", secondary=GenreArtist, back_populates="artists")
    tracks: List[models.Track] = relationship("Track", secondary=models.ArtistTrack, back_populates="artists")

    def __init__(self, name: str):
        self.name = name
