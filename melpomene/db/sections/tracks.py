from typing import List, Iterable

import sqlalchemy as sa
from sqlalchemy import orm as orm

from melpomene.db.models.track import Track
from melpomene.db.sections.base import DbApiSectionBase


class DbApiTracksSection(DbApiSectionBase):

    def get(self, track_spotify_id: str) -> Track:
        return self.session.query(Track).filter(Track.spotify_id == track_spotify_id).first()

    def get_all_spotify_ids(self) -> List[str]:
        return [t.spotify_id for t in self.session.query(Track).options(orm.load_only('spotify_id')).all()]

    def create_if_not_exist_and_get(self, track_spotify_id: str) -> Track:
        track = self.get(track_spotify_id)
        if track is None:
            track = Track(track_spotify_id)
            self.session.add(track)
        return track

    def get_count(self) -> int:
        return self.session.query(Track).count()

    def get_many_invalid(self, limit: int = 128, ignore_ids: List[int] = None) -> List[Track]:
        q = self.session.query(Track).filter(sa.or_(Track.name.is_(None), Track.info.is_(None)))
        if ignore_ids is not None:
            q = q.filter(Track.id.notin_(ignore_ids))
        return q.limit(limit).all()

    def get_invalid_count(self) -> int:
        return self.session.query(Track).filter(sa.or_(Track.name.is_(None), Track.info.is_(None))).count()

    def get_many_to_download(self, limit: int = 128, ignore_ids: List[int] = None) -> List[Track]:
        q = self.session.query(Track).filter(sa.and_(Track.preview_url.isnot(None), Track.preview_file.is_(None)))
        if ignore_ids is not None:
            q = q.filter(Track.id.notin_(ignore_ids))
        return q.limit(limit).all()

    def get_to_download_count(self) -> int:
        return self.session.query(Track).filter(sa.and_(Track.preview_url.isnot(None), Track.preview_file.is_(None)))\
            .count()

    def get_many_downloaded(self, limit: int = 128, offset: int = 0) -> List[Track]:
        return self.session.query(Track).filter(Track.preview_file.isnot(None)).offset(offset).limit(limit).all()

    def get_downloaded_count(self) -> int:
        return self.session.query(Track).filter(Track.preview_file.isnot(None)).count()

    def delete_many_by_spotify_ids(self, spotify_ids: Iterable[str]):
        self.session.query(Track).filter(Track.name.in_(spotify_ids)).delete()

    def search_by_name(self, search_string: str, limit: int = None, offset: int = None,
                       downloaded: bool = False) -> List[Track]:
        q = self.session.query(Track).filter(Track.name.ilike('%{}%'.format(search_string)))
        if downloaded:
            q = q.filter(Track.preview_file.isnot(None))
        if offset:
            q = q.offset(offset)
        if limit:
            q = q.limit(limit)
        return q.all()
