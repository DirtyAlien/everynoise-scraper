from typing import List

from melpomene.db.models import Artist
from melpomene.db.sections import DbApiSectionBase


class DbApiArtistsSection(DbApiSectionBase):

    def get(self, artist_name: str) -> Artist:
        return self.session.query(Artist).filter(Artist.name == artist_name).first()

    def get_all(self) -> List[Artist]:
        return self.session.query(Artist).all()

    def create_if_not_exist_and_get(self, artist_name: str) -> Artist:
        artist = self.get(artist_name)
        if artist is None:
            artist = Artist(artist_name)
            self.session.add(artist)
        return artist

    def get_count(self) -> int:
        return self.session.query(Artist).count()
