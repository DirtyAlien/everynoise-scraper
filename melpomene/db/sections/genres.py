from typing import List

from melpomene.db.models import Genre
from melpomene.db.sections.base import DbApiSectionBase


class DbApiGenresSection(DbApiSectionBase):

    def get(self, genre_name: str) -> Genre:
        return self.session.query(Genre).filter(Genre.name == genre_name).first()

    def get_all(self) -> List[Genre]:
        return self.session.query(Genre).all()

    def create_if_not_exist_and_get(self, genre_name: str) -> Genre:
        genre = self.get(genre_name)
        if genre is None:
            genre = Genre(genre_name)
            self.session.add(genre)
        return genre

    def get_count(self) -> int:
        return self.session.query(Genre).count()
