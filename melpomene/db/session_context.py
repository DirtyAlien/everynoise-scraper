import datetime

from sqlalchemy.orm import Session

import melpomene.db.sections as sections


class SessionContext:
    # NOTE: lifetime = None means that session is non-negotiating
    def __init__(self, session: Session, lifetime: datetime.timedelta = None):
        self.__session: Session = session
        self.__lifetime: datetime.timedelta = lifetime
        self.__expiration: datetime.datetime = None
        self.update_expiration()

        self.genres: sections.DbApiGenresSection = sections.DbApiGenresSection(self)
        self.artists: sections.DbApiArtistsSection = sections.DbApiArtistsSection(self)
        self.tracks: sections.DbApiTracksSection = sections.DbApiTracksSection(self)

    @property
    def session(self) -> Session:
        return self.__session

    @property
    def is_expired(self) -> bool:
        if self.__expiration is None:
            return False
        return datetime.datetime.now() > self.__expiration

    def update_expiration(self):
        if self.__lifetime is not None:
            self.__expiration = datetime.datetime.now() + self.__lifetime

    def close(self):
        try:
            self.__session.commit()
        except Exception as ex:
            print('Exception on commit: {}'.format(ex))
            self.__session.rollback()
        finally:
            self.__session.close()
