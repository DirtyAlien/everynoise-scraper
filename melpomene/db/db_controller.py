import datetime
import time
from threading import Thread
from typing import Dict

import sqlalchemy as sa
from sqlalchemy.orm import Session
from sqlalchemy_utils import database_exists, create_database

from melpomene.db.models import Base
from melpomene.db.session_context import SessionContext


class DbController:
    def __init__(self, clear: bool = False):
        self.session_contexts: Dict[int, SessionContext] = {}

        self.engine = sa.create_engine("postgres://postgres:postgres@localhost/music_database", client_encoding='utf8')
        if not database_exists(self.engine.url):
            create_database(self.engine.url)
        if clear:
            Base.metadata.drop_all(self.engine)
        Base.metadata.create_all(self.engine)

        self.__keep_session_expires_check_thread_alive = True
        self.__session_expires_check_thread: Thread = Thread(target=self.__session_expires_check_loop)
        self.__session_expires_check_thread.start()

    def get_session_context(self, user_id: int) -> SessionContext:
        session_context = self.session_contexts[user_id]
        if session_context is None:
            session_context = SessionContext(Session(self.engine), datetime.timedelta(minutes=10))
            self.session_contexts[user_id] = session_context
        else:
            session_context.update_expiration()
        return session_context

    def create_non_negotiating_session_context(self) -> SessionContext:
        return SessionContext(Session(self.engine))

    def stop_session_expires_check_loop(self):
        self.__keep_session_expires_check_thread_alive = False
        self.__session_expires_check_thread.join()

    def __session_expires_check_loop(self):
        while self.__keep_session_expires_check_thread_alive:
            for session_context in self.session_contexts.values():
                if session_context.is_expired:
                    session_context.close()
            time.sleep(.1)
